
data "aws_lambda_invocation" "example" {
  function_name = aws_lambda_function.default.function_name

  depends_on = [aws_lambda_function.default]
}

output "result_entry" {
  value = jsondecode(data.aws_lambda_invocation.example.result)["key1"]
}